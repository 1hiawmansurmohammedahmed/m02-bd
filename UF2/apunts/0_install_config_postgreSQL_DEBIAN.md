# Instal·lació i configuració del SGBDR postgreSQL i de la BD Training en Debian
---

_Important: si teniu dubtes de qui executa cada ordre, fixeu-vos en el prompt que hi ha al davant de cada ordre._


## 1. Instal·lació de programari
+ Iniciar una sessió com a usuari root (la contrassenya és la de sempre)
	```
	$ su -l
	```

+ Instal·lar el SGBDR postgreSQL (en Debian 11 instal·la la 13.3):
	```
	# apt-get install  postgresql postgresql-contrib
	```

+ Confirmar la instal·lació de paquets i dependències:
	```
	dpkg -l | grep postgres
	```

+ La mateixa instal·lació de Debian ja crea l'espai de dades necessari del SGBDR mitjançant un clúster de bases de dades gestionades per una instància. També crea l'usuari local _postgres_. Aquest usuari serà l'administrador (DBA) del servidor postgreSQL. Si us fixeu a la instal·lació això ja ho ha fet l'ordre:
	```
	pg_ctlcluster 13 main start
	```

	Podem mirar si el servei està funcionant posant _status_ en comptes de _start_ a l'ordre anterior.

+ Si encara tenim la info de la instal·lació també veurem que es mostra un directori i un fitxer de log:
	- `/var/lib/postgresql/13/main`: directori que conté fitxers de configuració
	- `/var/log/postgresql/postgresql-13-main.log`: fitxer on es registren totes les accions.

+ Podem comprovar que s'ha creat l'usuari postgres al fitxer /etc/passwd o executant l'ordre:
	```
	getent passwd postgres
	```
	Si no hi hagués línia de sortida, no existiria l'usuari localment.

+ Establim la contrasenya per a l'usuari postgres (li posem la mateixa que tenim per l'usuari root):
	```
	# passwd postgres
	```

* Debian ja inicia el servei per aquesta vegada i per totes. Si volguéssim parar o deshabilitar el servei ho hauríem de fer amb l'ordre `systemctl` i alguna de les opcions per habilitar, deshabilitar, iniciar, parar ... Nosaltres ara farem servir la que simplement dona informació de l'estat actual:

	```
	systemctl status postgresql
	```

+ Tanquem la sessió de root
	```
	# exit
	```

## 2. Creació del vostre usuari al servidor postgreSQL:

+ Inicieu una sessió amb l'usuari local _postgres_:
	```
	$ su -l postgres
	```

* El servidor postgreSQL per defecte crea un usuari de bases de dades que es diu _postgres_ i una base de dades que es diu _template1_. Amb el el programa client _psql_ ens hi connectarem:
	```
	$ psql template1
	```

+ Creeu el vostre usuari de bases de dades PostgreSQL amb el mateix login que feu servir per entrar al sistema operatiu:
	```
	=# CREATE USER el_vostre_usuari CREATEDB;
	```


* Exercici: com podem comprovar que s'ha creat el nostre usuari? Les ajudes de `psql` són o bé `man psql` o connectat a qualsevol base de dades `\?`. (Pista comença per `\d`) 


* Exercici: com ens desconnectem de la base de dades? un cop esbrinat feu-ho.

* Sortim també de la sessió bash de l'usuari _postgres_.

## 3. Creació de la base de dades training

+ Comproveu que esteu amb el `vostre usuari`
	```
	whoami
	```

+ Connecteu-vos a la base de dades `template1` amb el client `psql`
	```
	$ psql template1
	```

* Creeu la base de dades training
	```
	=> CREATE DATABASE training; # Algú sap perquè podeu crear bases de dades amb aquest usuari?
	```
	
+ Comproveu que s'ha creat la base de dades llistant les bases de dades disponibles:
	```
	=> \l
	```

+ Connecteu-vos a la base de dades Training:
	```
	=> \c training
	```

+ Executem l'ordres que hi ha al fitxer `trainingv3.sql`, el qual crearà l'estructura de la base de dades (taules) i carregarà les dades.
	```
	=> \i     /ruta/al_nostre/script.sql
	```

+ Visualitzem les taules de la base de dades
	```
	=> \d
	```

+ Visualitzem l'estructura d’una de les taules importades
	```
	=> \d OFICINA
	```

• Visualitzem les dades d’una de les taules importades:
	```
	=> SELECT * FROM OFICINA;
	```

+ Tanquem la connexió amb el servidor:
	```
	=> \q
	```

## 5. Connexió a la BD training amb el vostre usuari

Assegureu-vos que esteu connectats a la sessió de Debian amb el vostre usuari.

+ Ens connectem amb el client _psql_ a la base de dades _training_.
	```
	$ psql training
	```

+ Comproveu que podem llistar els continguts de les taules
	```
	=> SELECT * FROM OFICINA;
	```

+ Sortiu del client psql
	```
	=> \q
	```

## 6. Eliminació de la base de dades training del RDBMS PostgreSQL.

+ Iniciem una sessió amb l'usuari postgres
	```
	$ su -l postgres
	```

+ Ens connectem al servidor postgresql mitjançant el client _psql_, a la base de dades template1:

	```
	$ psql template1
	```

+ Exercici: elimineu la base de dades _training_:

+ Comprovem que ha desaparegut del llistat de bases de dades:
	```
	=> \l
	```

+ Sortim del gestor de bases de dades
	```
	=> \q
	```

+ Sortim de la sessió d'usuari administrador de PostgreSQL postgres
	```
	$ exit
	```

## 7. Desinstal·lació del servidor postgreSQL

+ Iniciem una sessió de root
	```
	$ su -l
	```

+ Aturem el servei postgresql
	```
	# systemctl stop postgresql
	```

+ Esborrem els paquets de PostgreSQL:
	```
	# apt-get purge postgresql postgresql-contrib 
	# apt-get autoremove 
	```

* Esborrem el directori on es guarden les bases de dades, l'ordre és perillosa i cal executar-la amb cura
	```
 	# rm -rf /var/lib/postgresql/13/main  
	```

* Sortim de la sessió del root:
	```
	# exit
	```

## 8. Executeu només una vegada

* Tornem a fer la instal·lació una segona vegada.

## 9. Ajuda i instal·lació de la documentació en local

* Investigueu quina informació proporcionen les comandes pròpies del client psql:
	```
	=> \h
	=> \?
	```

* Instal·leu com a root el paquet `postgresql-docs` per tenir la documentació en local.
	```
	$ su -l
	# apt-get install postgresql-doc
	```

+ Una vegada instal·lat el paquet, sortiu de la sessió de root
	```
	# exit
	```

+ A partir d'ara ja disposeu de la documentació en format HTML al directori `/usr/share/doc/postgresql-docs/html`. Per comprovar-ho, obriu l'index executant en consola la següent ordre:
	```
	$ firefox /usr/share/doc/postgresql-doc-13/html/index.html &
	```

## Solucions

+ `\du` o `\dg`
+ `exit` o `\q` o \<Ctrl\> + d
+ `DROP DATABASE training`
